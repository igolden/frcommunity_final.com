<?php
/*
* Template Name: Home Page
* */

get_header('home'); ?>
<div style="background: url('<?php echo the_field('homepage_image'); ?>') top center no-repeat; background-size: cover;" class="blue-bar-home">
<div class="row collapse">
<div class="large-12 columns" id="home-hero">
<h1 class="mt-50 text-center">
<strong>
<?php the_field('home_tagline'); ?>
</strong>
</h1>
<p class="text-center">
<br />
<a href="/communities" class="button round">Explore Our Communities</a>
</p>
</div>
</div>
</div>



<div id="wrapper">
	<div id="pager"></div>
	<div id="tabs">
		<div id="blue">
    <img src="<?php echo get_template_directory_uri(); ?>/img/relocation.jpg" alt="light_pink_muffin" width="151" height="245"/>
    <h3><?php the_field('tab1_title'); ?></h3>
			<p><?php the_field('tab1_content'); ?></p>
		</div>
		<div id="pink">
    <img src="<?php echo get_template_directory_uri(); ?>/img/pets.jpg" alt="light_pink_muffin" width="151" height="245"/>
			<h3><?php the_field('tab2_title'); ?></h3>
			<p><?php the_field('tab2_content'); ?></p>
		</div>
		<div id="green">
    <img src="<?php echo get_template_directory_uri(); ?>/img/home.jpg" alt="light_pink_muffin" width="151" height="245"/>
			<h3><?php the_field('tab3_title'); ?></h3>
			<p><?php the_field('tab3_content'); ?></p>
		</div>
		<div id="white">
    <img src="<?php echo get_template_directory_uri(); ?>/img/loyalty.jpg" alt="light_pink_muffin" width="151" height="245"/>
    <h3><?php the_field('tab4_title'); ?></h3>
			<p><?php the_field('tab4_content'); ?></p>
		</div>
	</div>
</div>
<div id="mobile-specials"
<div class="row mt-30">
<div class="large-12 columns">
    <h3><?php the_field('tab1_title'); ?></h3>
			<p><?php the_field('tab1_content'); ?></p>
<hr>
    <h3><?php the_field('tab2_title'); ?></h3>
			<p><?php the_field('tab2_content'); ?></p>
<hr>
    <h3><?php the_field('tab3_title'); ?></h3>
			<p><?php the_field('tab3_content'); ?></p>
<hr>
    <h3><?php the_field('tab4_title'); ?></h3>
			<p><?php the_field('tab4_content'); ?></p>
<hr>
</div>
</div>

</div>

<?php
get_footer('home');


