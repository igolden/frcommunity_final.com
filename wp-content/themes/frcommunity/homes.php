<?php
/*
* Template Name: Homes Home Page
* */

get_header('homes'); ?>
<div class="homes-bar">
<div class="row">
<div class="large-7 medium-8 columns" id="communities-home-info">
<h1><?php echo get_the_title(); ?></h1>
<p><?php the_field('homes_intro'); ?>
<br />
  <a class="fancybox-media hide-for-medium-up" href="http://www.youtube.com/watch?v=<?php echo the_field('homes_video'); ?>">Click Here to Watch Video</a>
</p>
</div>
<div class="large-4 large-offset-1 medium-4 columns hide-for-small" id="homes-video">
<iframe width="100%" height="250" class="mt-50" src="https://www.youtube.com/embed/<?php echo the_field('homes_video'); ?>" frameborder="0" allowfullscreen></iframe>
</div>
<div class="large-1 columns" id="community-slider">
</div>
</div>
</div>

<!--- gets code from content area -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php echo get_the_content(); ?>
<?php endwhile; else : ?>
	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<!--- Modals -->

<div id="homes-gallery" class="reveal-modal" data-reveal="">
<h4>Homes Gallery</h4>
<div class="row">
<?php 

$images = get_field('homes_photos');

if( $images ): ?>
    <ul class="small-block-grid-2 large-block-grid-4 medium-block-grid-4">
        <?php foreach( $images as $image ): ?>
            <li>
                <a href="<?php echo $image['url']; ?>" class="fancybox" rel="Model Photos">
                     <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
</div>
<a class="close-reveal-modal">&#215;</a>
</div>

<div id="kitchen-gallery" class="reveal-modal" data-reveal="">
<h4>Homes Interiors</h4>
<div class="row">
<?php 

$images = get_field('homes_interiors');

if( $images ): ?>
    <ul class="small-block-grid-2 large-block-grid-4 medium-block-grid-4">
        <?php foreach( $images as $image ): ?>
            <li>
                <a href="<?php echo $image['url']; ?>" class="fancybox" rel="Interior Photos">
                     <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
</div>
<a class="close-reveal-modal">&#215;</a>
</div>

<div id="floorplans" class="reveal-modal" data-reveal="">
<h4>Floorplans</h4>
<div class="row">
<?php 

$images = get_field('homes_floorplans');

if( $images ): ?>
    <ul class="small-block-grid-2 large-block-grid-4 medium-block-grid-4">
        <?php foreach( $images as $image ): ?>
            <li>
                <a href="<?php echo $image['url']; ?>" class="fancybox" rel="Floorplans">
                     <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
</div>
<a class="close-reveal-modal">&#215;</a>
</div>

<div id="bathroom-gallery" class="reveal-modal" data-reveal="">
<h4>Bathrooms</h4>
<div class="row">
<?php 

$images = get_field('homes_bathrooms');

if( $images ): ?>
    <ul class="small-block-grid-2 large-block-grid-4 medium-block-grid-4">
        <?php foreach( $images as $image ): ?>
            <li>
                <a href="<?php echo $image['url']; ?>" class="fancybox" rel="Bathrooms">
                     <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
</div>
<a class="close-reveal-modal">&#215;</a>
</div>
<?php if( have_rows('homes_for_sale', 5) ): ?>
<div class="row mt-30">
<h2>Current Available Homes:</h2>
</div>
<div class="row">
<div class="large-12 medium-12 columns">
<h3 id="bayview">Bayview Homes</h3>
<hr>
</div>
</div>


	<?php while( have_rows('homes_for_sale', 5) ): the_row(); 

		// vars
		$title = get_sub_field('homes_title');
		$image = get_sub_field('homes_image');
		$desc = get_sub_field('homes_description');
		$price = get_sub_field('homes_price');
		$sqft = get_sub_field('square_feet');
		$rental = get_sub_field('homes_rental');

		?>
	<div class="row mt-30">

		<div class="large-3 columns">

    <a href="<?php echo $image ?>" class="fancybox">
				<img src="<?php echo $image ?>">
</a>
    </div>
      <div class="large-6 columns">
      <h4 class="homes-title"><?php echo $title  ?></h4>
      <p><?php echo $desc ?>
</p>
    <strong>	
<?php if( ( $price) ): ?>
  $<?php echo $price ?>
<?php else: ?>
  <?php echo 'Call for Price' ?>
<?php endif; ?>

 | <?php echo $sqft ?> sq.ft. 

<?php if( ( $rental) ): ?>
| Rental: $<?php echo $rental ?>
<?php else: ?>

<?php endif; ?>

</strong>



</div>
<div class="large-3 columns">
<p><a href="tel:<?php echo the_field('community_phone', 5); ?>" class="small button round">Call Sales</a>
 <a href="mailto:<?php echo the_field('community_email', 5); ?>" class="small button round">Email Sales</a></p>
</div>
	</div>

	<?php endwhile; ?>

<?php endif; ?>


<?php if( have_rows('homes_for_sale', 35) ): ?>
<div class="row">
<div class="large-12 medium-12 columns">
<hr>
<h3 id="eagles-nest">Eagle's Nest Homes</h3>
<hr>
</div>
</div>


	<?php while( have_rows('homes_for_sale', 35) ): the_row(); 

		// vars
		$title = get_sub_field('homes_title');
		$image = get_sub_field('homes_image');
		$desc = get_sub_field('homes_description');
		$price = get_sub_field('homes_price');
		$sqft = get_sub_field('square_feet');
		$rental = get_sub_field('homes_rental');

		?>
	<div class="row mt-30">

		<div class="large-3 columns">

    <a href="<?php echo $image ?>" class="fancybox">
				<img src="<?php echo $image ?>">
</a>
    </div>
      <div class="large-6 columns">
      <h4 class="homes-title"><?php echo $title  ?></h4>
      <p><?php echo $desc ?>
</p>
		<strong>	
<?php if( ( $price) ): ?>
  $<?php echo $price ?>
<?php else: ?>
  <?php echo 'Call for Price' ?>
<?php endif; ?>

 | <?php echo $sqft ?> sq.ft. 

<?php if( ( $rental) ): ?>
| Rental: $<?php echo $rental ?>
<?php else: ?>

<?php endif; ?>

</strong>



</div>
<div class="large-3 columns">
<p><a href="tel:<?php echo the_field('community_phone', 35); ?>" class="small button round">Call Sales</a>
 <a href="mailto:<?php echo the_field('community_email', 35); ?>" class="small button round">Email Sales</a></p>
</div>
	</div>

	<?php endwhile; ?>

<?php endif; ?>
<div class="row">
<div class="large-3 medium-3 columns">
</div>
<div class="large-6 medium-6 columns">
</div>
<div class="large-3 medium-3 columns">
</div>
</div>


<?php if( have_rows('homes_for_sale', 48) ): ?>

<div class="row">
<div class="large-12 medium-12 columns">
<hr>
<h3 id="whispering-pines">Whispering Pines Homes</h3>
<hr>
</div>
</div>


	<?php while( have_rows('homes_for_sale', 48) ): the_row(); 

		// vars
		$title = get_sub_field('homes_title');
		$image = get_sub_field('homes_image');
		$desc = get_sub_field('homes_description');
		$price = get_sub_field('homes_price');
		$sqft = get_sub_field('square_feet');
		$rental = get_sub_field('homes_rental');

		?>
	<div class="row mt-30">

		<div class="large-3 columns">

    <a href="<?php echo $image ?>" class="fancybox">
				<img src="<?php echo $image ?>">
</a>
    </div>
      <div class="large-6 columns">
      <h4 class="homes-title"><?php echo $title  ?></h4>
      <p><?php echo $desc ?>
</p>
    <strong>	
<?php if( ( $price) ): ?>
  $<?php echo $price ?>
<?php else: ?>
  <?php echo 'Call for Price' ?>
<?php endif; ?>

 | <?php echo $sqft ?> sq.ft. 

<?php if( ( $rental) ): ?>
| Rental: $<?php echo $rental ?>
<?php else: ?>

<?php endif; ?>

</strong>



</div>
<div class="large-3 columns">
<p><a href="tel:<?php echo the_field('community_phone', 48); ?>" class="small button round">Call Sales</a> <a href="mailto:<?php echo the_field('community_email', 48); ?>" class="small button round">Email Sales</a></p>
</div>
	</div>

	<?php endwhile; ?>

<?php endif; ?>
<div class="row">
<div class="large-3 medium-3 columns">
</div>
<div class="large-6 medium-6 columns">
</div>
<div class="large-3 medium-3 columns">
</div>
</div>


<?php if( have_rows('homes_for_sale', 112) ): ?>
<div class="row">
<div class="large-12 medium-12 columns">
<hr>
<h3 id="oakwood">Oakwood Homes</h3>
<hr>
</div>
</div>


	<?php while( have_rows('homes_for_sale', 112) ): the_row(); 

		// vars
		$title = get_sub_field('homes_title');
		$image = get_sub_field('homes_image');
		$desc = get_sub_field('homes_description');
		$price = get_sub_field('homes_price');
		$sqft = get_sub_field('square_feet');
		$rental = get_sub_field('homes_rental');

		?>
	<div class="row mt-30">

		<div class="large-3 columns">

    <a href="<?php echo $image ?>" class="fancybox">
				<img src="<?php echo $image ?>">
</a>
    </div>
      <div class="large-6 columns">
      <h4 class="homes-title"><?php echo $title  ?></h4>
      <p><?php echo $desc ?>
</p>
    <strong>	
<?php if( ( $price) ): ?>
  $<?php echo $price ?>
<?php else: ?>
  <?php echo 'Call for Price' ?>
<?php endif; ?>

 | <?php echo $sqft ?> sq.ft. 

<?php if( ( $rental) ): ?>
| Rental: $<?php echo $rental ?>
<?php else: ?>

<?php endif; ?>

</strong>



</div>
<div class="large-3 columns">
<p><a href="tel:<?php echo the_field('community_phone', 112); ?>" class="small button round">Call Sales</a> <a href="mailto:<?php echo the_field('community_email', 112); ?>" class="small button round">Email Sales</a></p>
</div>
	</div>

	<?php endwhile; ?>

<?php endif; ?>
<div class="row">
<div class="large-3 medium-3 columns">
</div>
<div class="large-6 medium-6 columns">
</div>
<div class="large-3 medium-3 columns">
</div>
</div>







<?php if( have_rows('homes_for_sale', 32) ): ?>
<div class="row">
<div class="large-12 medium-12 columns">
<hr>
<h3 id="shiloh">Shiloh Homes</h3>
<hr>
</div>
</div>


	<?php while( have_rows('homes_for_sale', 32) ): the_row(); 

		// vars
		$title = get_sub_field('homes_title');
		$image = get_sub_field('homes_image');
		$desc = get_sub_field('homes_description');
		$price = get_sub_field('homes_price');
		$sqft = get_sub_field('square_feet');
		$rental = get_sub_field('homes_rental');

		?>
	<div class="row mt-30">

		<div class="large-3 columns">

    <a href="<?php echo $image ?>" class="fancybox">
				<img src="<?php echo $image ?>">
</a>
    </div>
      <div class="large-6 columns">
      <h4 class="homes-title"><?php echo $title  ?></h4>
      <p><?php echo $desc ?>
</p>
    <strong>	
<?php if( ( $price) ): ?>
  $<?php echo $price ?>
<?php else: ?>
  <?php echo 'Call for Price' ?>
<?php endif; ?>

 | <?php echo $sqft ?> sq.ft. 

<?php if( ( $rental) ): ?>
| Rental: $<?php echo $rental ?>
<?php else: ?>

<?php endif; ?>
</strong>



</div>
<div class="large-3 columns">
<p><a href="tel:<?php echo the_field('community_phone', 32); ?>" class="small button round">Call Sales</a> <a href="mailto:<?php echo the_field('community_email', 32); ?>" class="small button round">Email Sales</a></p>
</div>
	</div>

	<?php endwhile; ?>

<?php endif; ?>
<div class="row">
<div class="large-3 medium-3 columns">
</div>
<div class="large-6 medium-6 columns">
</div>
<div class="large-3 medium-3 columns">
</div>
</div>

<?php if( have_rows('homes_for_sale', 45) ): ?>
<div class="row">
<div class="large-12 medium-12 columns">
<hr>
<h3 id="stonegate">Stonegate Homes</h3>
<hr>
</div>
</div>


	<?php while( have_rows('homes_for_sale', 45) ): the_row(); 

		// vars
		$title = get_sub_field('homes_title');
		$image = get_sub_field('homes_image');
		$desc = get_sub_field('homes_description');
		$price = get_sub_field('homes_price');
		$sqft = get_sub_field('square_feet');
		$rental = get_sub_field('homes_rental');

		?>
	<div class="row mt-30">

		<div class="large-3 columns">

    <a href="<?php echo $image ?>" class="fancybox">
				<img src="<?php echo $image ?>">
</a>
    </div>
      <div class="large-6 columns">
      <h4 class="homes-title"><?php echo $title  ?></h4>
      <p><?php echo $desc ?>
</p>
    <strong>	
<?php if( ( $price) ): ?>
  $<?php echo $price ?>
<?php else: ?>
  <?php echo 'Call for Price' ?>
<?php endif; ?>

 | <?php echo $sqft ?> sq.ft. 

<?php if( ( $rental) ): ?>
| Rental: $<?php echo $rental ?>
<?php else: ?>

<?php endif; ?>
</strong>



</div>
<div class="large-3 columns">
<p><a href="tel:<?php echo the_field('community_phone', 45); ?>" class="small button round">Call Sales</a> <a href="mailto:<?php echo the_field('community_email', 45); ?>" class="small button round">Email Sales</a></p>
</div>
	</div>

	<?php endwhile; ?>

<?php endif; ?>
<div class="row">
<div class="large-3 medium-3 columns">
</div>
<div class="large-6 medium-6 columns">
</div>
<div class="large-3 medium-3 columns">
</div>
</div>



<?php if( have_rows('homes_for_sale', 37) ): ?>
<div class="row">
<div class="large-12 medium-12 columns">
<hr>
<h3 id="tallgrass">Tallgrass Homes</h3>
<hr>
</div>
</div>


	<?php while( have_rows('homes_for_sale', 37) ): the_row(); 

		// vars
		$title = get_sub_field('homes_title');
		$image = get_sub_field('homes_image');
		$desc = get_sub_field('homes_description');
		$price = get_sub_field('homes_price');
		$sqft = get_sub_field('square_feet');
		$rental = get_sub_field('homes_rental');

		?>
	<div class="row mt-30">

		<div class="large-3 columns">

    <a href="<?php echo $image ?>" class="fancybox">
				<img src="<?php echo $image ?>">
</a>
    </div>
      <div class="large-6 columns">
      <h4 class="homes-title"><?php echo $title  ?></h4>
      <p><?php echo $desc ?>
</p>
    <strong>	
<?php if( ( $price) ): ?>
  $<?php echo $price ?>
<?php else: ?>
  <?php echo 'Call for Price' ?>
<?php endif; ?>

 | <?php echo $sqft ?> sq.ft. 

<?php if( ( $rental) ): ?>
| Rental: $<?php echo $rental ?>
<?php else: ?>

<?php endif; ?>
</strong>



</div>
<div class="large-3 columns">
<p><a href="tel:<?php echo the_field('community_phone', 37); ?>" class="small button round">Call Sales</a> <a href="mailto:<?php echo the_field('community_email', 37); ?>" class="small button round">Email Sales</a></p>
</div>
	</div>

	<?php endwhile; ?>

<?php endif; ?>
<div class="row">
<div class="large-3 medium-3 columns">
</div>
<div class="large-6 medium-6 columns">
</div>
<div class="large-3 medium-3 columns">
</div>
</div>





<?php if( have_rows('homes_for_sale', 43) ): ?>
<div class="row">
<div class="large-12 medium-12 columns">
<hr>
<h3 id="twinleaf">Twinleaf Homes</h3>
<hr>
</div>
</div>


	<?php while( have_rows('homes_for_sale', 43) ): the_row(); 

		// vars
		$title = get_sub_field('homes_title');
		$image = get_sub_field('homes_image');
		$desc = get_sub_field('homes_description');
		$price = get_sub_field('homes_price');
		$sqft = get_sub_field('square_feet');
		$rental = get_sub_field('homes_rental');

		?>
	<div class="row mt-30">

		<div class="large-3 columns">

    <a href="<?php echo $image ?>" class="fancybox">
				<img src="<?php echo $image ?>">
</a>
    </div>
      <div class="large-6 columns">
      <h4 class="homes-title"><?php echo $title  ?></h4>
      <p><?php echo $desc ?>
</p>
    <strong>
<?php if( ( $price) ): ?>
  $<?php echo $price ?>
<?php else: ?>
  <?php echo 'Call for Price' ?>
<?php endif; ?>

 | <?php echo $sqft ?> sq.ft. 

<?php if( ( $rental) ): ?>
| Rental: $<?php echo $rental ?>
<?php else: ?>

<?php endif; ?>
</strong>



</div>
<div class="large-3 columns">
<p><a href="tel:<?php echo the_field('community_phone', 43); ?>" class="small button round">Call Sales</a> <a href="mailto:<?php echo the_field('community_email', 43); ?>" class="small button round">Email Sales</a></p>
</div>
	</div>

	<?php endwhile; ?>

<?php endif; ?>
<div class="row">
<div class="large-3 medium-3 columns">
</div>
<div class="large-6 medium-6 columns">
</div>
<div class="large-3 medium-3 columns">
</div>
</div>




   




 
<?php
get_footer();
