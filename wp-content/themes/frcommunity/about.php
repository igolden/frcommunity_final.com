<?php
/*
* Template Name: About Page
* */

get_header('2'); ?>
<div class="about-bar">
<div class="row">
<div class="large-8 medium-12 columns" id="communities-home-info">
<h1><?php echo get_the_title(); ?></h1>

<p>
<?php the_field('about_intro'); ?>
</p>

</div>
<div class="large-4 medium-4 hide-for-small hide-for-medium columns mt-50">
<img src="<?php the_field('about_image'); ?>">
</div>
</div>
</div>


   

<div class="row mt-30">
<div class="large-10 large-offset-1 medium-offset-1 medium-10 columns comm-title">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<p><?php echo get_the_content(); ?></p>

<?php endwhile; else : ?>
	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

</div>
<div class="large-1 medium-1 columns">

</div>
</div>


 
<?php
get_footer();
