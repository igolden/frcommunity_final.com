<?php
/*
* Template Name: Community Page
* */

get_header('2'); ?>

<div style="background: url('<?php echo the_field('header_image'); ?>') top center;" id="community-slider" class="blue-bar">
<!---
<div class="large-6 columns" id="community-info">
<h1><?php echo get_the_title(); ?></h1>
<p><img src="<?php echo get_template_directory_uri(); ?>/img/call-icon.png" id="call-icon"><?php the_field('community_phone'); ?></p>
<p><img src="<?php echo get_template_directory_uri(); ?>/img/address.png" id="address-icon"><?php the_field('community_address'); ?><br />
<span style="margin-left: 58px;"><?php the_field('community_zip'); ?></span></p>
<p><img src="<?php echo get_template_directory_uri(); ?>/img/email.png" id="manager-icon">Manager: <?php the_field('manager'); ?></p>
</div>
 --->

<div id="white-top-bar">
<div class="row">
<div class="large-6 medium-6 columns">
<h1><?php echo get_the_title(); ?></h1>
<p><a href="#call-to-action">CONTACT US</a> | <a href="<?php the_field('brochure_file'); ?>">DOWNLOAD BROCHURE</a> | <a href="<?php echo the_field('community_app'); ?>" target="new">DOWNLOAD APPLICATION</a>
</p>
</div>

<div id="white-top-bar-right" class="large-6 medium-6 columns hide-for-small hide-for-xs">
<h1><?php the_field('community_phone'); ?></h1>
<p><?php the_field('community_address'); ?>, <?php the_field('community_zip'); ?></p>
</div>

</div>
</div>
<!--- End White Top Bar --->
<div class="row">
<div id="tagline" class="large-12 medium-12 columns">
<h1><?php the_field('community_title'); ?></h1>
</div>
</div>
<!--- End Tagline --->


<a href="#features-overview" class="hide-for-small">
<img src="/custom-content/scroll.png" class="hide-for-small">
</a>
<!--- End scroll --->

</div>
<!--- End Community Slider --->
<div  id="sticky-nav-community">
<div class="row mt-10" style="position: relative;">
<div class="large-6 medium-6 columns">
<h4><?php echo get_the_title(); ?></h4>
</div>
<div class="large-6 medium-6 columns hide-for-small">
<p><a href="#call-to-action">CONTACT US</a> | <a href="<?php the_field('brochure_file'); ?>">DOWNLOAD BROCHURE</a> | <a href="mailto:<?php the_field('community_email'); ?>">EMAIL MANAGER</a>
</p>
</div>
</div>
<div class="row" id="custom-hr" class="hide-for-small"></div>
<div class="row">
<div class="large-8 medium-8 columns">
<h3><a href="#features-overview">Homes & Pricing</a> <span>/</span> <a href="#features-list">Features</a> <span>/</span> <a href="#image-gallery">Photos</a> <span>/</span> <a href="#call-to-action">Location</a>
</h3>
</div>
<div class="large-4 medium-4 columns text-right ">
<h3 class="text-right hide-for-small"><?php the_field('community_phone'); ?>
</h3>
</div>
</div>
</div>
   

<div class="row mt-30" style="position: relative;">
  <div id="content" class="large-9 medium-9 columns">
  <h2 id="about"><?php the_field('community_title'); ?>.</h2>
<div class="community-content">
<?php the_field('community_summary'); ?>
</div>
</div>
  <div id="content" class="large-3 medium-3 columns">
<a href="/homes/">
<img src="/custom-content/see-homes.png">
<p class="text-center">
<br />

Own Your Next Home at <?php echo get_the_title(); ?>
</p>
</a>
</div>
</div>
<div style="background: url('<?php echo the_field('middle_hero'); ?>') top center; background-size: cover;" class="mt-50" id="features-overview">

<div id="amenities" class="row collapse">
<div class="large-4 medium-4 medium-offset-2 large-offset-2 columns" id="half-white">
<ul class="flist">
<li><h2><a class="active" data-orbit-link="headline-1">Our Homes</a></h2></li>
<!---
<li><h2><a data-orbit-link="headline-2">Policies</a></h2></li>
--->
<li><h2><a data-orbit-link="headline-3">Community Amenities</a></h2></li>
<li><h2><a data-orbit-link="headline-4">Promotions</a></h2></li>
</ul>
<hr class="show-for-small">
</div>
<div class="large-6 medium-6 columns" id="almost-white">
<ul data-orbit
    data-options="animation:slide;
                  pause_on_hover:false;
                  animation_speed:500;
                  navigation_arrows:false;
                  bullets:false;
                  next_on_click: false;
      slide_number: false,
      slide_number_text: false,
      timer: false;
      timer_speed: 9999999999;
autoplay: false;

">
  <li data-orbit-slide="headline-1">

    <div>
<!--- Add Funcitonality -->
<h3>Our Homes</h3>
<h5><?php echo get_the_title(); ?> homes for rent, sale, and vacant sites available.</h5>

<?php if( have_rows('rooms_pricing') ): ?>

	<table class="table">

	<?php while( have_rows('rooms_pricing') ): the_row(); 

		// vars
		$rooms = get_sub_field('rooms_bed');
		$sqft = get_sub_field('rooms_sqft');
		$price = get_sub_field('rooms_price');

		?>

                            <tr>
                            <td class="model"><?php echo $rooms ?></td>
                                <td class="sqft"><?php echo $sqft ?> sq. ft.</td>
                                <td class="from">From</span>: $<span class="price"><?php echo $price ?></span>


                                </td>
                            </tr>

	<?php endwhile; ?>

	</table>

<?php endif; ?>

<a href="/homes#<?php echo strtolower(get_the_title()); ?>/">See Available <?php echo get_the_title(); ?> Homes for sale.
</a>

</div>
  </li>
<!---
  <li data-orbit-slide="headline-2">
    <div>
<h3>Policies</h3>
<?php the_field('policies'); ?>
    </div>
  </li>
-->
  <li data-orbit-slide="headline-3">
    <div>
      <h3>Community Amenities</h3>
<?php the_field('amenities_list'); ?>
    </div>
  </li>
  <li data-orbit-slide="headline-4">
    <div>
      <h3>Promotions</h3>
<?php if( have_rows('promotions') ): ?>

<?php the_field('promo_text'); ?>

	<?php while( have_rows('promotions') ): the_row(); 

		// vars
		$title = get_sub_field('promo_title');
		$file = get_sub_field('promo_file');
		$desc = get_sub_field('promo_desc');

		?>

<p>
<strong><?php echo $title ?></strong>
<br />
<?php echo $desc ?>
<br />
<a href="<?php echo $file ?>">Download PDF</a>
</p>

	<?php endwhile; ?>

	</table>

<?php else: ?>

<p>Sorry, no current promotions</p>

<?php endif; ?>



    </div>
  </li>

</ul>
</div>
</div>
</div>
<div id="features-list">
<div class="row">
<div class="large-6 medium-6 columns">
<h4>Promotions</h4>
<?php if( have_rows('promotions') ): ?>

<?php the_field('promo_text'); ?>

	<?php while( have_rows('promotions') ): the_row(); 

		// vars
		$title = get_sub_field('promo_title');
		$file = get_sub_field('promo_file');
		$desc = get_sub_field('promo_desc');

		?>

<p>
<strong><?php echo $title ?></strong>
<br />
<?php echo $desc ?>
<br />
<a href="<?php echo $file ?>">Download PDF</a>
</p>

	<?php endwhile; ?>

	</table>

<?php else: ?>

<p>Sorry, no current promotions</p>

<?php endif; ?>



</div>
<div class="large-6 medium-6 columns">
<h4>Community Amenities</h4>
<?php the_field('amenities_list'); ?>

</div>
</div>
</div>
<div id="bottom-hero" style="background: url('<?php echo the_field('bottom_hero'); ?>') top center no-repeat; background-size: cover;">
<h1 class="shadow"><?php the_field('community_title'); ?></h1>
<p><?php the_field('bottom_hero_desc'); ?></p>
</div>
<div id="image-gallery">
<div class="row mt-30">
<h4>Community Gallery</h4>
</div>
<?php 

$images = get_field('community_gallery');

if( $images ): ?>
<ul id="gallery" class="large-block-grid-6 small-block-grid-3">
        <?php foreach( $images as $image ): ?>
        <li data-ls="slidedelay: 2500; transition2d: 5;">
       <a href="<?php echo $image['url']; ?>" class="fancybox" rel="<?php echo the_title(); ?>"> <img src="<?php echo $image['url']; ?>" ></a> 
      </li>
        <?php endforeach; ?>
      </ul>
<?php endif; ?>



</div>
<div id="call-to-action">

<div class="row">
<h3>Talk to a Leasing Agent</h3>
<div class="large-6 medium-6 columns">
<?php
$form = get_field('contact_form');
if($form)
{
  echo do_shortcode( $form ); 
}
?>
</div>
<div class="large-6 medium-6 columns">

<h4><?php echo the_title(); ?></h4>
<p>
<?php the_field('community_address'); ?>
 <?php the_field('community_zip'); ?>
<br />
  <?php the_field('community_phone'); ?>
<br />
  <?php the_field('community_email'); ?>
</p>
<p>
<a href="<?php echo the_field('community_facebook'); ?>" target="new" title="Facebook"><i class="blue fa fa-facebook-square fa-2x"></i></a>
</p>
<img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php the_field('community_address'); ?>&zoom=14&size=400x200
&markers=size:large%7Ccolor:red%7C<?php the_field('community_address'); ?>
">

</div>
</div>

 
<?php
get_footer('community');
