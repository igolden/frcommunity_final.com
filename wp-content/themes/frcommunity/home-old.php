<?php
/*
* Template Name: Home Page
* */

get_header('home'); ?>
<div class="blue-bar-home">
<div class="row collapse">
<div class="large-12 columns">
<?php 

$images = get_field('slider_images');

if( $images ): ?>
<div id="layerslider" style="width: 100%; height: 450px;">
        <?php foreach( $images as $image ): ?>
        <div class="ls-slide" data-ls="slidedelay: 2500; transition2d: 5;">
       <img src="<?php echo $image['url']; ?>" class="ls-bg" alt="Slide background">
  
      </div>
        <?php endforeach; ?>
      </div>
<?php endif; ?>

</div>
</div>
</div>



<div id="wrapper">
	<div id="pager"></div>
	<div id="tabs">
		<div id="blue">
    <img src="<?php echo get_template_directory_uri(); ?>/img/relocation.png" alt="light_pink_muffin" width="151" height="245"/>
			<h3>We'll Help You Relocate</h3>
			<p>Cupcake ipsum dolor sit amet pie pudding tiramisu. Powder biscuit oat cake jelly-o jelly beans danish cookie. Jelly carrot cake topping souffl&eacute; topping brownie apple pie lemon drops sweet roll. Gummi bears danish lollipop. Jelly-o chocolate bar halvah jelly beans. Bonbon sesame snaps wafer. Applicake croissant donut sugar plum drag&eacute;e. Sweet oat cake danish.</p>
		</div>
		<div id="pink">
    <img src="<?php echo get_template_directory_uri(); ?>/img/pet_policy.png" alt="light_pink_muffin" width="151" height="245"/>
			<h3>Bring Your Pets!</h3>
			<p>Sesame snaps tootsie roll donut. Sweet roll candy canes chocolate bar wafer sweet roll gummies topping gummi bears. Pudding cheesecake cookie. Muffin sweet roll tart tart muffin. Pastry pudding marshmallow cupcake cake toffee toffee bonbon topping. Apple pie cupcake caramels. Icing chocolate bar lemon drops caramels bonbon bear claw.</p>
		</div>
		<div id="green">
    <img src="<?php echo get_template_directory_uri(); ?>/img/rent.png" alt="light_pink_muffin" width="151" height="245"/>
			<h3>Own Your Own Home</h3>
			<p>Apple pie jelly gingerbread gummies topping. Gingerbread jelly marshmallow oat cake wafer lollipop. Cupcake chocolate cake apple pie. Caramels lollipop croissant bear claw brownie carrot cake wafer halvah lollipop. Candy canes tootsie roll jelly-o. Liquorice sugar plum sweet icing tiramisu.</p>
		</div>
		<div id="white">
    <img src="<?php echo get_template_directory_uri(); ?>/img/family.png" alt="light_pink_muffin" width="151" height="245"/>
			<h3>Satisfaction Guaranteed</h3>
			<p>Apple pie jelly gingerbread gummies topping. Gingerbread jelly marshmallow oat cake wafer lollipop. Cupcake chocolate cake apple pie. Caramels lollipop croissant bear claw brownie carrot cake wafer halvah lollipop. Candy canes tootsie roll jelly-o. Liquorice sugar plum sweet icing tiramisu.</p>
		</div>
	</div>
</div>

<?php
get_footer('home');


