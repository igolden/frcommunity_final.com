<?php
/*
* Template Name:  Contact Page Template
* */

get_header('2'); ?>
<div class="row mt-30">
<div class="large-4 columns">
<h2><?php echo the_field('corporate_name'); ?></h2>
<p>
<strong>Address:</strong>
<br />
<?php echo the_field('corporate_address'); ?>
<br />
<?php echo the_field('corporate_zip'); ?>
<br />
<strong>Phone:</strong> <?php echo the_field('corporate_phone'); ?>
<br />
<strong>Fax:</strong> <?php echo the_field('corporate_fax'); ?>
<br />
<strong>Email:</strong> <?php echo the_field('corporate_email'); ?>
</p>
<hr>
<img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php the_field('corporate_address'); ?>&size=350x300&zoom=14
&markers=size:large%7Ccolor:red%7C<?php the_field('corporate_address'); ?>
" width="200" height="500">

</div>
<div class="large-8 columns">
<?php
$form = get_field('contact_page_form');
if($form)
{
  echo do_shortcode( $form ); 
}
?>
</div>
<div class="large-12 columns">
<!--- gets code from content area -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php echo get_the_content(); ?>
<?php endwhile; else : ?>
	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>


</div>
</div>
<?php
get_footer();

