<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

</section> 


 
<div class="footer mt-50">
<footer id="home-footer"> 
<div class="row mt-30" id="last-words">
<hr>
<div class="large-2 medium-2 columns">
<img src="<?php the_field('footer_logo', 'option'); ?>">
</div>
<div class="large-10 medium-10 columns">
<p style="padding-top: 20px;"><?php the_field('footer_tagline', 'option'); ?></p>
<br />

</div>
</div>
<div id="copyright" class="row">
<div class="large-6 large-offset-3 columns text-center">
<p class="text-center">
<?php 
$images = get_field('footer_logos', 'options');

if( $images ): ?>
        <?php foreach( $images as $image ): ?>
                     <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />  
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
<br />
  © Copyright 2015 FRCommunity.com. All Rights Reserved.
<br />
<a href="http://denverwebco.com">Denver Web Design</a>
</p>
</div>
</div>
</footer>
</div>

<div id="myModal" class="reveal-modal" data-reveal="">

  <h4>Contact us using the form below</h4>

  <div id="contact-form">
    <label>Name</label>
    <input name="name" type="text">

    <label>Email</label>
    <input name="email" type="text">

    <label>Phone</label>
    <input name="phone" type="text">

    <label>Questions</label>
    <textarea rows="10" name="name" type="text"></textarea>

  </div>
 <p><a href="#" data-reveal-id="secondModal" class="secondary button">Send</a></p>
  <a class="close-reveal-modal">×</a>
</div>    
<div id="secondModal" class="reveal-modal" data-reveal="">
    <h2>Thank you.</h2>
      <p>We will contact you soon if necessary. </p>
        <a class="close-reveal-modal">×</a>
      </div>

<!-- smooth scroll -->
<script>
  $(function() {
    $('a[href*=#]:not([href=#])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        }
      }
    });
  });
</script>

<script src="<?php echo get_template_directory_uri(); ?>/js/foundation.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.slicknav.min.js"></script>
<script>
	$(function(){
		$('#main').slicknav();
	});
</script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.carouFredSel-6.0.4-packed.js" type="text/javascript"></script>
</script>
		<script type="text/javascript">
			$(function() {
				$('#tabs').carouFredSel({
					circular: false,
					items: 1,
					width: '100%',
					auto: false,
					pagination: {
						container: '#pager',
						anchorBuilder: function( nr ) {
							return '<a href="#">' + $(this).find('h3').text() + '</a>';
						}
					}
				});
			});
		</script>


<!-- Fancybox -->
	<?php wp_footer(); ?>

</body>
</html>
