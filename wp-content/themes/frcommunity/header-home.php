<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
  <!-- FR Scripts -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/foundation.css">
    <!-- LayerSlider stylesheet -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/js/layerslider/css/layerslider.css" type="text/css">
     
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/slicknav.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/custom.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css">
    <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/modernizr.js"></script>



  <meta class="foundation-data-attribute-namespace"><meta class="foundation-mq-xxlarge"><meta class="foundation-mq-xlarge"><meta class="foundation-mq-large"><meta class="foundation-mq-medium"><meta class="foundation-mq-small"><style></style><meta class="foundation-mq-topbar"><link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/layerslider/skins/v5/skin.css" type="text/css">

	<?php wp_head(); ?>
</head>



<body> 
    <header>
<div class="top-bar-color">
<div class="row collapse">
<div class="large-12 columns">
    <div class="contain-to-grid sticky">
      <nav class="top-bar" data-topbar>
<ul class="title-area">
<li>
<!--
<a href="#" class="active">English</a> | 
<a href="#" class="active">Español</a>
-->
<li>
</li>
  <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
</ul>
<section class="top-bar-section">
<ul id="icons-top" class="right">
<li><a href="#" title="Facebook"><i class="fa fa-facebook fa-2x"></i></a></li>
<li><a href="#" data-reveal-id="myModal" title="Email Us"><img src="<?php echo get_template_directory_uri(); ?>/img/email-icon.png"></a></li>
<li><a href="#" title="Call Us"><img src="<?php echo get_template_directory_uri(); ?>/img/phone-icon.png"></a></li>
</ul>

</section>

</nav>
</div>
</div>
</div>
</div>

<!--- Logo Area -->
<div class="row" id="logo-area">
  <div class="large-4 columns">
    <a href="/" title="FR Community">
    <img src="<?php the_field('header_logo', 'options'); ?>">
  </a>
  </div>
  <div class="large-8 columns">
<section class="top-bar-section">
<ul id="main" class="left">
<li>
<a href="/" class="">Home</a>
</li>
<li>
<a href="/communities" class="">Our Communities</a>
</li>
<li><a href="/homes">Our Homes</a></li>
<li class="has-dropdown not-click"><a href="/about">About</a>
<ul class="dropdown"><li class="title back js-generated"><h5><a href="javascript:void(0)">Back</a></h5></li><li class="parent-link show-for-small"><a class="parent-link js-generated" href="#">Communities</a></li>
<li><a href="/investors">Investor Relations</a></li>
<li><a href="/employment">Employment</a></li>
</ul>
</li>
<li><a href="/contact">Contact</a></li>
</ul>
</section>
</div>

</div>
<!-- End Logo Area -->

</header>

<section id="main-content">
