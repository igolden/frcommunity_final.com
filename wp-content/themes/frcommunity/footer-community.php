<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

</section> 


 
<div class="footer mt-30">
<footer> 
<div class="row">
  <div class="large-6 medium-8 columns">
    <strong> Communities</strong><br>
<ul id="communities-block" class="small-block-grid-2 medium-block-grid-3 large-block-grid-3">
<li><a href="/bayview">Bayview</a></li>
<li><a href="/eagles-nest">Eagles Nest</a></li>
<li><a href="/oakwood">Oakwood</a></li>
<li><a href="/shiloh">Shiloh</a></li>
<li><a href="/stonegate">Stonegate</a></li>
<li><a href="/tallgrass">Tallgrass</a></li>
<li><a href="/twinleaf">Twinleaf</a></li>
<li><a href="/whispering-pines">Whispering Pines</a></li>
</ul>
<div class="mt-20">
    <strong>Links</strong><br>
<ul id="links-block" class="small-block-grid-2 medium-block-grid-4 large-block-grid-5">
<li class="mt-20"><a href="/">Home</a></li>
<li class="mt-20"><a href="/about">About</a></li>
<li class="mt-20"><a href="/homes">Our Homes</a></li>
<li class="mt-20"><a href="/contact">Contact</a></li>
</ul>
  </div>
  </div>
  <div class="large-3 medium-4 columns">
    <strong>Connect with Us</strong>
<ul id="social-block" class="small-block-grid-6">
<li class="mt-20"><a href="#" title="Facebook"><i class="fa fa-facebook fa-2x"></i></a></li>
<li class="mt-20"><a href="#" title="Call Us"><img src="<?php echo get_template_directory_uri(); ?>/img/phone-icon.png"></a></li>
<li class="mt-20"><a href="#" data-reveal-id="myModal" title="Email Us"><img src="<?php echo get_template_directory_uri(); ?>/img/email-icon.png"></a></li>
</ul>
  <div class="show-for-medium">
    <strong>Contact</strong>
    <br>
Phone: 303-794-6655
    <br>
Fax: 303-790-0928
    <br>
    info@frcommunity.com
  </div>
  

  </div>
  <div class="large-3 columns hide-for-medium hide-for-small">
    <strong>Contact</strong>
    <br>
    303-123-2345
    <br>
    info@frcommunity.com
  </div>
</div>
<hr>
<div class="row mt-30" id="last-words">
<div class="large-2 medium-2 columns">
<img src="<?php the_field('footer_logo', 'option'); ?>">
</div>
<div class="large-10 medium-10 columns">
<p style="padding-top: 20px;"><?php the_field('footer_tagline', 'option'); ?></p>
</div>
</div>
<div id="copyright" class="row">
<div class="large-6 large-offset-3 columns text-center">
<p class="text-center">
<?php 
$images = get_field('footer_logos', 'options');

if( $images ): ?>
        <?php foreach( $images as $image ): ?>
                     <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />  
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
<br />
  © Copyright 2015 FRCommunity.com. All Rights Reserved.
<br />
<a href="http://denverwebco.com">Denver Web Design</a>
</p>
</div>
</div>
</footer>
</div>
<div class="show-for-small" id="contact-button">
<a href="tel:<?php the_field('community_phone'); ?>">Call Us Now</a></div>

<div id="myModal" class="reveal-modal" data-reveal="">

  <h4>Contact us using the form below</h4>

  <div id="contact-form">
    <label>Name</label>
    <input name="name" type="text">

    <label>Email</label>
    <input name="email" type="text">

    <label>Phone</label>
    <input name="phone" type="text">

    <label>Questions</label>
    <textarea rows="10" name="name" type="text"></textarea>

  </div>
 <p><a href="#" data-reveal-id="secondModal" class="secondary button">Send</a></p>
  <a class="close-reveal-modal">×</a>
</div>    
<div id="secondModal" class="reveal-modal" data-reveal="">
    <h2>Thank you.</h2>
      <p>We will contact you soon if necessary. </p>
        <a class="close-reveal-modal">×</a>
      </div>
<!-- smooth scroll -->
<script>
  $(function() {
    $('a[href*=#]:not([href=#])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top - 115
          }, 1000);
          return false;
        }
      }
    });
  });
</script>

<script>
$('ul.flist li a').click(function () {
    $('ul.flist li a.active').removeClass('active');
+          $(this).addClass('active');});
</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/foundation.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/foundation/foundation.magellan.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.sticky-kit.min.js"></script>
    <script>
      $(document).foundation();
    </script>

<script>
  $(document).ready(function() {
    $("#sticky-nav-community").stick_in_parent({offset_top: 0,})
    .on('sticky_kit:bottom', function(e) {
          $(this).parent().css('position', 'static');
        })
});
</script>

<!-- External libraries: jQuery & GreenSock -->
<script src="<?php echo get_template_directory_uri(); ?>/js/layerslider/js/greensock.js" type="text/javascript"></script>
 
<!-- LayerSlider script files -->
<script src="<?php echo get_template_directory_uri(); ?>/js/layerslider/js/layerslider.transitions.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
    <script type="text/javascript"> 
      // Running the code when the document is ready
    $(document).ready(function(){

        // Calling LayerSlider on the target element
        $('#layerslider').layerSlider({


          pauseOnHover: false,
          });
        });
 
                      </script>

<!-- Fancybox -->

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>  
<script type="text/javascript">
$('.fancybox').fancybox({
  padding: 0,
    helpers: {
      overlay: {
        locked: false
      }
    }
});
</script>

	<?php wp_footer(); ?>

</body>
</html>
