<?php
/*
* Template Name: Communities Home Page
* */

get_header('2'); ?>
<div class="blue-bar">
<div class="row">
<div class="large-6 medium-6 large-offset-1 columns" id="communities-home-info">
<h1><?php echo get_the_title(); ?></h1>
<p><?php the_field('communities_intro'); ?></p>
</div>
<div class="large-4 medium-4 hide-for-small columns" id="community-layerslider">
<?php 

$images = get_field('comm_slider');

if( $images ): ?>
<div id="layerslider" style="width: 100%; height: 220px;">
        <?php foreach( $images as $image ): ?>
        <div class="ls-slide" data-ls="slidedelay: 2500; transition2d: 5;">
       <img src="<?php echo $image['url']; ?>" class="ls-bg" alt="Slide background">
  
      </div>
        <?php endforeach; ?>
      </div>
<?php endif; ?>

</div>
<div class="large-1 columns" id="community-slider">
</div>
</div>
</div>


   

<div class="row mt-30">
<div class="large-10 medium-10 large-offset-1 medium-offset-1 columns comm-title">
<h2>Find a Community Near You:</h2>
<?php echo do_shortcode('[gmw form="1"]'); ?>
</div>
</div>
<div class="row mt-30">
<div class="large-10 medium-10 large-offset-1 medium-offset-1 columns">
<div class="row">
<div class="large-12 columns comm-title">
<h2>Indiana</h2>
<hr>
</div>
</div>
<div class="row mt-20">
<!-- Bayview -->
<div class="large-3 medium-3 columns comm-list">
<h4><a href="/bayview">Bayview</a></h4>
<p><?php the_field('community_address', 5); ?><br />
<?php the_field('community_zip', 5); ?>
</p>
<p><?php the_field('community_phone', 5); ?></p>
<p>
<a href="<?php echo the_field('community_facebook', 5); ?>" target="new" title="Facebook"><i class="blue fa fa-facebook-square fa-2x"></i></a>
</p>
</div>


<!-- Eagle's Nest -->
<div class="large-3 medium-3 columns comm-list">
<h4><a href="/eagles-nest">Eagle's Nest</a></h4>
<p><?php the_field('community_address', 35); ?><br />
<?php the_field('community_zip', 35); ?>
</p>
<p><?php the_field('community_phone', 35); ?></p>
<p>
<a href="<?php echo the_field('community_facebook', 35); ?>" target="new" title="Facebook"><i class="blue fa fa-facebook-square fa-2x"></i></a>
</p>
</div>



<!-- Shiloh -->
<div class="large-3 medium-3 columns comm-list">
<h4><a href="/shiloh">Shiloh</a></h4>
<p><?php the_field('community_address', 32); ?><br />
<?php the_field('community_zip', 32); ?>
</p>
<p><?php the_field('community_phone', 32); ?></p>
<p>
<a href="<?php echo the_field('community_facebook', 32); ?>" target="new" title="Facebook"><i class="blue fa fa-facebook-square fa-2x"></i></a>
</p>
</div>


<!-- Whispering Pines -->
<div class="large-3 medium-3 columns comm-list">
<h4><a href="/whispering-pines">Whispering Pines</a></h4>
<p><?php the_field('community_address', 48); ?><br />
<?php the_field('community_zip', 48); ?>
</p>
<p><?php the_field('community_phone', 48); ?></p>
<p>
<a href="<?php echo the_field('community_facebook', 48); ?>" target="new" title="Facebook"><i class="blue fa fa-facebook-square fa-2x"></i></a>
</p>
</div>



</div>



<div class="row mt-40">
<div class="large-12 columns comm-title">
<h2>Kansas</h2>
<hr>
</div>
</div>
<div class="row mt-20">
<!-- Stonegate -->
<div class="large-3 medium-3 columns comm-list">
<h4><a href="/stonegate">Stonegate</a></h4>
<p><?php the_field('community_address', 45); ?><br />
<?php the_field('community_zip', 45); ?>
</p>
<p><?php the_field('community_phone', 45); ?></p>
<p>
<a href="<?php echo the_field('community_facebook', 45); ?>" target="new" title="Facebook"><i class="blue fa fa-facebook-square fa-2x"></i></a>
</p>
</div>


<div class="large-3 medium-3 columns comm-list">
</div>



<div class="large-3 medium-3 columns comm-list">
</div>
<div class="large-3 medium-3 columns comm-list">
</div>
</div>


<div class="row mt-40">
<div class="large-12 columns comm-title">
<h2>Michigan</h2>
<hr>
</div>
</div>

<div class="row mt-20">
<!-- Tall Grass -->
<div class="large-3 medium-3 columns comm-list">
<h4><a href="/tallgrass">Tallgrass</a></h4>
<p><?php the_field('community_address', 37); ?><br />
<?php the_field('community_zip', 37); ?>
</p>
<p><?php the_field('community_phone', 37); ?></p>
<p>
<a href="<?php echo the_field('community_facebook', 37); ?>" target="new" title="Facebook"><i class="blue fa fa-facebook-square fa-2x"></i></a>
</p>
</div>

<!-- Twinleaf -->
<div class="large-3 medium-3 columns comm-list">
<h4><a href="/twinleaf">Twinleaf</a></h4>
<p><?php the_field('community_address', 43); ?><br />
<?php the_field('community_zip', 43); ?>
</p>
<p><?php the_field('community_phone', 43); ?></p>
<p>
<a href="<?php echo the_field('community_facebook', 43); ?>" target="new" title="Facebook"><i class="blue fa fa-facebook-square fa-2x"></i></a>
</p>
</div>

<div class="large-3 medium-3 columns comm-list">
</div>
<div class="large-3 medium-3 columns comm-list">
</div>
</div>



<div class="row mt-40">
<div class="large-12 columns comm-title">
<h2>Illinois</h2>
<hr>
</div>
</div>

<div class="row mt-20">
<!-- Oakwood -->
<div class="large-3 medium-3 columns comm-list">
<h4><a href="/oakwood">Oakwood</a></h4>
<p><?php the_field('community_address', 112); ?><br />
<?php the_field('community_zip', 112); ?>
</p>
<p><?php the_field('community_phone', 112); ?></p>
<p>
<a href="<?php echo the_field('community_facebook', 112); ?>" target="new" title="Facebook"><i class="blue fa fa-facebook-square fa-2x"></i></a>
</p>
</div>

<div class="large-3 medium-3 columns comm-list">
</div>
<div class="large-3 medium-3 columns comm-list">
</div>
<div class="large-3 medium-3 columns comm-list">
</div>
</div>

 
<?php
get_footer();
