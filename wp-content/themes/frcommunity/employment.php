<?php
/*
 * Template Name:  Employment Page
 * */

get_header('2'); ?>
<div class="page-bar">
<div class="row">
<div class="large-6 large-offset-1 columns" id="communities-home-info">
<h1><?php echo get_the_title(); ?></h1>
<p><?php the_field('employment_intro'); ?></p>
</div>
<div class="large-4 columns" id="community-layerslider">
<img src="<?php echo the_field('employment_image'); ?>">
</div>
<div class="large-1 columns" id="community-slider">
</div>
</div>
</div>




<div class="row mt-30">
<div class="large-10 medium-10 large-offset-1 medium-offset-1 columns">
<div class="row">
<div class="large-12 columns comm-title">
<h2>Meet Our Managers</h2>
<hr>
</div>
</div>
<div class="row mt-20">
<!-- Bayview -->
<div class="large-3 medium-3 columns comm-list">
<h4><a href="/bayview">Bayview</a></h4>
<p><strong><?php echo the_field('community_manager', 5); ?></strong></p>
<p><a href="tel:<?php the_field('community_phone', 5); ?>"><?php the_field('community_phone', 5); ?></a></p>
<p><a href="mailto:<?php echo the_field('community_email', 5); ?>">Send Email</a>
</p>
</div>


<!-- Shiloh -->
<div class="large-3 medium-3 columns comm-list">
<h4><a href="/shiloh">Shiloh</a></h4>
<p><strong><?php echo the_field('community_manager', 32); ?></strong></p>
<p><a href="tel:<?php the_field('community_phone', 32); ?>"><?php the_field('community_phone', 32); ?></a></p>
<p><a href="mailto:<?php echo the_field('community_email', 32); ?>">Send Email</a>
</p>
</div>


<!-- Whispering Pines -->
<div class="large-3 medium-3 columns comm-list">
<h4><a href="/whispering-pines">Whispering Pines</a></h4>
<p><strong><?php echo the_field('community_manager', 48); ?></strong></p>
<p><a href="tel:<?php the_field('community_phone', 48); ?>"><?php the_field('community_phone', 48); ?></a></p>
<p><a href="mailto:<?php echo the_field('community_email', 48); ?>">Send Email</a>
</p>
</div>


<!-- Eagle's Nest -->
<div class="large-3 medium-3 columns comm-list">
<h4><a href="/eagles-nest">Eagle's Nest</a></h4>
<p><strong><?php echo the_field('community_manager', 35); ?></strong></p>
<p><a href="tel:<?php the_field('community_phone', 35); ?>"><?php the_field('community_phone', 35); ?></a></p>
<p><a href="mailto:<?php echo the_field('community_email', 35); ?>">Send Email</a>
</p>
</div>



</div>

<div class="row mt-40">
<!-- Stonegate -->
<div class="large-3 medium-3 columns comm-list">
<h4><a href="/stonegate">Stonegate</a></h4>
<p><strong><?php echo the_field('community_manager', 45); ?></strong></p>
<p><a href="tel:<?php the_field('community_phone', 45); ?>"><?php the_field('community_phone', 45); ?></a></p>
<p><a href="mailto:<?php echo the_field('community_email', 45); ?>">Send Email</a>
</p>
</div>



<!-- Twinleaf -->
<div class="large-3 medium-3 columns comm-list">
<h4><a href="/twinleaf">Twinleaf</a></h4>
<p><strong><?php echo the_field('community_manager', 43); ?></strong></p>
<p><a href="tel:<?php the_field('community_phone', 43); ?>"><?php the_field('community_phone', 43); ?></a></p>
<p><a href="mailto:<?php echo the_field('community_email', 43); ?>">Send Email</a>
</p>
</div>

<!-- Tall Grass -->
<div class="large-3 medium-3 columns comm-list">
<h4><a href="/tallgrass">Tallgrass</a></h4>
<p><strong><?php echo the_field('community_manager', 37); ?></strong></p>
<p><a href="tel:<?php the_field('community_phone', 37); ?>"><?php the_field('community_phone', 37); ?></a></p>
<p><a href="mailto:<?php echo the_field('community_email', 37); ?>">Send Email</a>
</p>
</div>

<!-- Oakwood -->
<div class="large-3 medium-3 columns comm-list">
<h4><a href="/oakwood">Oakwood</a></h4>
<p><strong><?php echo the_field('community_manager', 112); ?></strong></p>
<p><a href="tel:<?php the_field('community_phone', 112); ?>"><?php the_field('community_phone', 112); ?></a></p>
<p><a href="mailto:<?php echo the_field('community_email', 112); ?>">Send Email</a>
</p>
</div>
</div>

<hr>

<div class="row mt-30">
<div class="large-12 medium-12 columns">
<h3>Interested in Employment?</h3>

<?php while( have_rows('jobs', 187) ): the_row(); 

// vars
$title = get_sub_field('job_title');
$desc = get_sub_field('job_desc');
$app = get_sub_field('job_app');
?>
  <div class="row mt-30">

		<div class="large-3 columns">

<strong>
<?php echo $title ?>
</strong>
    </div>

      <div class="large-6 columns">
      <?php echo $desc ?>
</div>
<div class="large-3 columns">
     <a class="button small round" href="<?php echo $app ?>" target="new">
Print Application
</a>
</div>
<hr>
	</div>

	<?php endwhile; ?>



<?php
get_footer();

