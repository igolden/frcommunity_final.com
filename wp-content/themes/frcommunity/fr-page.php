<?php
/*
* Template Name:  FR Page Template
* */

get_header('2'); ?>
<div class="page-bar">
<div class="row">
<div class="large-7 medium-7 columns" id="communities-home-info">
<h1><?php echo get_the_title(); ?></h1>
<p><?php the_field('page_intro'); ?></p>
</div>
<div class="large-1 medium-1 columns">
<p></p>
</div>
<div class="large-4 medium-4 columns" id="community-layerslider">
<img src="<?php echo the_field('page_image'); ?>">
</div>
</div>
</div>


   

<div class="row mt-30">
<div class="large-12 columns">


<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php echo get_the_content(); ?>

<?php endwhile; else : ?>

	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

<?php endif; ?>


</div>
</div>
<?php
get_footer();

