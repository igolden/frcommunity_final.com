<?php
/*
Plugin Name: FR Community
Plugin URI: http://indywebco.com/
Description: Declares a plugin that will create a custom post type for each community.
Version: 1.0
Author: Ian Golden
Author URI: http://indywebco.com/
License: GPLv2
*/

// Registers the new post type and taxonomy

function wpt_event_posttype() {
	register_post_type( 'vehicles',
		array(
			'labels' => array(
				'name' => __( 'Communities' ),
				'singular_name' => __( 'Vehicle' ),
				'add_new' => __( 'Add New Vehicle' ),
				'add_new_item' => __( 'Add New Vehicle' ),
				'edit_item' => __( 'Edit Item' ),
				'new_item' => __( 'Add New Vehicle' ),
				'view_item' => __( 'View Item' ),
				'search_items' => __( 'Search Inventory' ),
				'not_found' => __( 'Nothing matches in Inventory' ),
				'not_found_in_trash' => __( 'Nothing found in trash' )
			),
			'public' => true,
			'supports' => array( 'title' ),
			'capability_type' => 'post',
			'rewrite' => array("slug" => "vehicles"), // Permalinks format
      'menu_position' => 5,
      'menu_icon' => '/wp-content/plugins/vehicles/images/icon.png',
			'register_meta_box_cb' => 'add_events_metaboxes'
		)
	);
}
// Change the columns for the edit CPT screen
function change_columns( $cols ) {
  $cols = array(
    'cb'       => '<input type="checkbox" />',
    'thumbnail'      => __( 'Thumbnail',      'trans' ),
    'year'      => __( 'Year',      'trans' ),
    'make' => __( 'Make', 'trans' ),
    'model'     => __( 'Model', 'trans' ),
    'price'     => __( 'Price', 'trans' ),
    'miles'     => __( 'Miles', 'trans' ),
    'on_hold'     => __( 'On Hold', 'trans' ),
    'on_hold_date'     => __( 'Hold Date', 'trans' ),
    'titled'     => __( 'Titled', 'trans' ),
    'title'      => __( 'Page Link',      'trans' ),
  );
  return $cols;
}
add_filter( "manage_vehicles_posts_columns", "change_columns" );
function custom_columns( $column, $post_id ) {
  switch ( $column ) {
    case "thumbnail":
      $thumbnail = get_post_meta( $post_id, 'thumbnail', true);
      echo '<a href="' . get_edit_post_link() . '">';
$args = array(
	'numberposts' => 1,
	'order'=> 'ASC',
	'post_mime_type' => 'image',
	'post_parent' => $post_id,
	'post_type' => 'attachment'
	);

$get_children_array = get_children($args,ARRAY_A);  //returns Array ( [$image_ID]... 
$rekeyed_array = array_values($get_children_array);
$child_image = $rekeyed_array[0];  


  echo "<img src=\"";
  echo $child_image['guid'];   	//Show the $child_image ID.
      echo "\" width=\"120px\">";
      echo '</a>';
      break;
    case "year":
      $year = get_post_meta( $post_id, 'year', true);
      echo $year;
      break;
    case "make":
      $make = get_post_meta( $post_id, 'make', true);
      echo $make;
      break;
    case "model":
      $model = get_post_meta( $post_id, 'model', true);
      echo $model;
      break;
    case "price":
      $price = get_post_meta( $post_id, 'price', true);
      echo $price;
      break;
    case "miles":
      $miles = get_post_meta( $post_id, 'miles', true);
      echo $miles;
      break;
    case "on_hold":
      $on_hold = get_post_meta( $post_id, 'on_hold', true);
      echo $on_hold;
      break;
    case "on_hold_date":
      $on_hold_date = get_post_meta( $post_id, 'hold_date', true);
      echo $on_hold_date;
      break;
    case "titled":
      $titled = get_post_meta( $post_id, 'titled', true);
      echo $titled;
      break;
    case "url":
      $url = get_post_meta( $post_id, 'url', true);
      echo '<a href="' . admin_url() . '">' . $url. '</a>';
      break;
  }
}
function myposttype_admin_script($hook_suffix) {

		global $typenow; if ($typenow=="vehicles") {

			echo  "<script src='" . get_template_directory_uri() .  "/js/admin-script.js' ></script>";
			echo  "<script src='" . get_template_directory_uri() .  "/js/admin-jquery.js' ></script>";
			echo  "<link rel='stylesheet' href='" . get_template_directory_uri() .  "/css/admin-styles.css'>";
		}

	}

	add_action('admin_enqueue_scripts', 'myposttype_admin_script');

add_action( "manage_posts_custom_column", "custom_columns", 10, 2 );
add_action( 'init', 'wpt_event_posttype' );



add_filter( 'media_upload_tabs', 'cwwp_remove_media_tabs' );
/**
 * Removes specified media tabs from the media uploader.
 *
 * @since 1.0.0
 *
 * @param array $tabs Default media upload tabs
 * @return array $tabs Amended media upload tabs
 */
function cwwp_remove_media_tabs( $tabs ) {
	
	/** Simply uncomment any of the following to remove them from the media uploader */
	//unset( $tabs['type'] ); // This removes the "From Computer" tab
	//unset( $tabs['type_url'] ); // This removes the "From URL" tab
	//unset( $tabs['gallery'] ); // This removes the "Gallery" tab
unset( $tabs['library'] ); // This remove the "Media Library" tab
		
	/** Return the amended tabs array */
	return $tabs;
	
}
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}
