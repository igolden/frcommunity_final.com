<?php
/*
* Template Name:  Contact Page Template
* */

get_header('2'); ?>
<div class="row mt-30">
<div class="large-4 columns">
<?php echo the_field('contact_page_form'); ?>
<?php echo the_field('corporate_address'); ?>
<?php echo the_field('corporate_phone'); ?>
<?php echo the_field('corporate_email'); ?>
</div>
<div class="large-8 columns">
<?php echo the_field(); ?>
</div>
<div class="large-12 columns">
<!--- gets code from content area -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php echo get_the_content(); ?>
<?php endwhile; else : ?>
	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>


</div>
</div>
<?php
get_footer();

